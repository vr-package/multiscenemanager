using System.Collections;
using System.Linq.Expressions;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoadManager : MonoBehaviour
{
    [SerializeField]
    int[] scenesBuildIndex = new int[] { 6, 7 };
    [SerializeField] int setActiveSceneIndex = -1;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(LoadLevels());
    }
    IEnumerator LoadLevels()
    {

        enabled = false;

        for (int i = 0; i < scenesBuildIndex.Length; i++)
        {
            int sceneIndex = scenesBuildIndex[i];
            var x = SceneManager.GetSceneByBuildIndex(sceneIndex);
            if (x != null && x.isLoaded) continue;

            yield return SceneManager.LoadSceneAsync(
               sceneIndex, LoadSceneMode.Additive);

            if(setActiveSceneIndex== i)
            {
                x = SceneManager.GetSceneByBuildIndex(sceneIndex);
                SceneManager.SetActiveScene(x);
            }
        }
        yield return null;
        enabled = true;
    }

}
